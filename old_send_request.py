#!/usr/bin/python3
import sys,socket
import requests
import os
import subprocess
import json
import argparse

def get_token():
    result = subprocess.run(['/home/control/Workspace/Client/token.sh'], stdout=subprocess.PIPE)
    data = json.loads(result.stdout)
    print (data['access_token'])
    return data['access_token']


parser = argparse.ArgumentParser(description='tool to send get and post request')
parser.add_argument('-u', action="store", dest="uri")
parser.add_argument('-r', action="store", dest="request_type")
parser.add_argument('-d', action="store", dest="post_data")

my_args = parser.parse_args()
if (my_args.request_type == "get"):
    r = requests.get('https://52.11.38.213:8443/appsone-controlcenter/'+my_args.uri , headers={'Authorization':  get_token()}, verify=False)

if (my_args.request_type == "post"):
    f = open(my_args.post_data, 'r')
    json_data = f.read()
    r = requests.post('https://52.11.38.213:8443/appsone-controlcenter/'+my_args.uri , data=json_data,headers={'Authorization':  get_token()}, verify=False)

#r = requests.get('https://52.11.38.213:8443/appsone-controlcenter/v1.0/api/configData', headers={'Authorization': 
#        'eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJYeldqTWZBV25zaGpGUHFWcDdiRWNjMkZ2b0d4TDBCSjhzMnVhazFUUWd3In0.eyJqdGkiOiJhMDI4YzdkZC0zNjFmLTQwOWItODY4ZC01ZjczZGIyMTgwNWMiLCJleHAiOjE1ODA0NTkyMTMsIm5iZiI6MCwiaWF0IjoxNTgwNDU5MTUzLCJpc3MiOiJodHRwczovLzUyLjExLjM4LjIxMzo4NDQzL2F1dGgvcmVhbG1zL21hc3RlciIsInN1YiI6Ijc2NDAxMjNhLWZiZGUtNGZlNS05ODEyLTU4MWNkMWUzYTljMSIsInR5cCI6IkJlYXJlciIsImF6cCI6ImFkbWluLWNsaSIsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9zdGF0ZSI6IjY3OGI2MzhkLWRiMDktNGJiMS05OTUxLWM0MGE3NmI4NDM4NiIsImFjciI6IjEiLCJzY29wZSI6InByb2ZpbGUgZW1haWwiLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsInByZWZlcnJlZF91c2VybmFtZSI6ImFwcHNvbmVhZG1pbiJ9.F1xM-yCVS7D6HyczyOGHogXlAwJjg-vrQ6dZ62qIAKyIYVcE5ZVrbhLCWNr8eOXxkEAwL5HUc6kgvyoFXmqQPu6dNBkiG3LN8HehHk5w4VvUSx6l5IYa0Hsw7hAID-AGjsR43wqDCPi_R6SFLWR4jaSCWNgKAJ79FVE0PoRbc6thPkHOmrxaMxWQA7E8T_q8sAEIT3SesMtrFGgH_d-sjsfLTAdsnDrwszc2ycLTWkZ2oq7Us3mIR4VHrTmxnmO_5UIVvuNuVXRegHk_u-RON2CPqODSWGP24jMF9s50-Pq-WsrcQKu-TPMNW13YRnF7-KMubLwI9eK8NWjte7hm7Q'
#        }, verify=False)
print(r.content)
print('status: {0}'.format(r.status_code))
