#!/usr/bin/python3
import sys,socket
import requests
import os
import subprocess
import json
import argparse
import time
import colorama
from termcolor import colored 
  
colorama.init() 

base_url = "https://52.11.38.213:8443/appsone-controlcenter/"
first_pass = False


def get_token():
    result = subprocess.run(['./token.sh'], stdout=subprocess.PIPE)
    data = json.loads(result.stdout)
    return data['access_token']

def clean_db():
    result = subprocess.run(['./clean_db.sh'], stdout=subprocess.PIPE)
    return

def load_config(config_file):
    f = open(config_file, 'r')
    data_json = json.loads(f.read())
    f.close()
    return data_json

def create_log_dir():
     result = subprocess.run(['mkdir -p ./logs'], stdout=subprocess.PIPE)


def send_request(request_type, uri, data):
    global KPI
    if request_type == "get":
        r = requests.get(base_url + uri , headers={'Authorization':  get_token()}, verify=False)
    else :
        f = open(data, 'r')
        json_data = f.read()
        f.close()
        global first_pass
        if ("addCluster" in uri) and (not first_pass):
            print ("Current KPI:{0}".format(KPI))
            first_pass = True
            json_data = json_data.replace("{0}", KPI)
            print("json data\n{0}".format(json_data))
        r = requests.post(base_url + uri , data=json_data,headers={'Authorization':  get_token()}, verify=False)
    if str(r.status_code) == "200":
        print(colored("Success {0}/{1}\n {2}".format(base_url, uri, r.content), 'green'))
        if "addKPI" in uri:
            response_data = json.loads(r.content)['data']
            for k,v in response_data.items():
                print("KPI:{0}".format(k))
                KPI = k
                f = open("logs/kpidata", "w")
                f.write(str(r.content))
                f.close()
    else:
        print(colored("Failed:{0} {1}/{2}".format(r.status_code, base_url, uri), 'red'))
        print(r.content)
        try:
            print("\t\t{0}".format(json.loads(r.content)['data']))
            f = open("logs/"+uri.rsplit('/', 1)[-1], "w")
            f.write("status code: {0}".format(r.status_code))
            f.write("Content: {0}".format(r.content))
            f.close()
        except:
            print("In except")
        
KPI=None
parser = argparse.ArgumentParser(description='tool to send get and post request')
parser.add_argument('-f', action="store", dest="config_file")
my_args = parser.parse_args()

clean_db()
config_file = load_config(my_args.config_file)
#config_file_json = json.load(config_file)
print("size of configfile {0}".format(len(config_file['requests'].items())))
total_no_of_requests = len(config_file['requests'].items())
for i in range(total_no_of_requests):
    #curr_config = config_file_json['requests'][str(i)]
    curr_config = config_file['requests'][str(i)]
    print("Sending request for {0}", curr_config['name'])
    send_request(curr_config['request_type'], curr_config['uri'], curr_config['data'])
    time.sleep(10)

